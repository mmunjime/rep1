{
  "headers": {
    "authorization": "Bearer h0r8Kx343wa-GUPGvy-H2rGlaQNWsM8sGuOpsNAkXOId3nz9DQXVq_WaGLiS1epXZ2EVhHmN9jm4owJC-HfKZQ",
    "client-id": "7ca53be88fa44fb8b6f5080d77d64837",
    "x-correlation-id": "31c451d5-3ea9-46fc-9927-bc073fd97a9a",
    "user-agent": "PostmanRuntime/7.28.0",
    "accept": "*/*",
    "postman-token": "026d0541-6af4-49ce-8659-0d4c5137cca4",
    "host": "localhost:8092",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {
    "mode": "verbose"
  },
  "requestUri": "/api/ping?mode=verbose",
  "queryString": "mode=verbose",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/ping",
  "listenerPath": "/api/*",
  "relativePath": "/api/ping",
  "localAddress": "/127.0.0.1:8092",
  "uriParams": {},
  "rawRequestUri": "/api/ping?mode=verbose",
  "rawRequestPath": "/api/ping",
  "remoteAddress": "/127.0.0.1:57389",
  "requestPath": "/api/ping"
}